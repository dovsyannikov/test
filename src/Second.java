import java.util.Scanner;

public class Second {
    public static void main(String[] args) {
//        System.out.println(in3050(32, 35));
//        System.out.println(in3050_second(32, 111));
//        ternarOperand(5,4);
//        switchCase();
        example();
        table();
    }

    public static void example(){
        for (int i = 1; i <= 7; i++) {
            System.out.println(i +  " я выполнюсь перед continue");
            if (i > 2){
                continue;
            }
            System.out.println(i + ": я выполюсь 2 раза");
        }
    }


//    public static boolean in3050(int a, int b) {
//        if ((a >= 30 && a <= 40) && (b >= 30 && b <= 40)) {
//            return true;
//        } else if ((a >= 40 && a <= 50) && (b >= 40 && b <= 50)) {
//            return true;
//        } else {
//            return false;
//        }
//    }
//
//    public static void ternarOperand(int a, int b){
//        System.out.println(a==b ? "a и b are equals" : "a и b are not equals");
//    }
//    public static boolean in3050_second(int a, int b) {
//        if(((a>=30 && a<=40) && (b>=30 && b<=40)) || ((a>=40 && a<=50) && (b>=40 && b<=50))) {
//            return true;
//        }
//        return false;
//    }
//    public static void switchCase(){
//        System.out.println("Enter number of day:");
//        Scanner scanner = new Scanner(System.in);
//        int day = Integer.parseInt(scanner.next());
//        scanner.close();
//
//        switch (day) {
//            case 1:
//                System.out.println("Monday");
//                break;
//            case 2:
//                System.out.println("Tuesday");
//                break;
//            case 3:
//                System.out.println("Wednesday");
//            case 4:
//                System.out.println("Thursday");
//            case 5:
//                System.out.println("Friday");
//                break;
//            case 6:
//                System.out.println("Saturday");
//                break;
//            case 7:
//                System.out.println("Sunday");
//                break;
//            default:
//                System.out.println("I can't find");
//        }
//        System.out.println(day);
//
//    }
//    private static void ifElseEx() {
//        int a = 3;
//        int b = 7;
//        boolean c = a == b;
//
//        if (c = false){
//            System.out.println("a!=b");
//        } else {
//            System.out.println("a==b");
//        }
//    }
//
//    public static void sum(String message, int ...nums){
////        System.out.println(message);
////        int result = 0;
////        for (int x:nums) {
////            result+=x;
////            System.out.println(result);
////        }
//    }
    public static void table(){
        for (int i = 1; i <= 10; i++) {
            System.out.println("3 * " + i + " = " + 3*i);
        }
    }
}
